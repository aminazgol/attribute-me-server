const joi = require('joi');
module.exports = {
    schemaValidation(schema, req, res, next) {
    const { error } = joi.validate(req, schema);

    if (error)
        return res.code(400).json({
            message: "Bad request",
            error: error
        })

    next();
    }
}