/* operational errors caused by client */
class OpError extends Error {
    constructor (code, message, error) {
        if(error){
            super(message+ ": " +error.message)
            Error.captureStackTrace(error)
        }
        else{
            super(message)
            Error.captureStackTrace(this)
        }
        this.code = code

        Object.setPrototypeOf(this, new.target.prototype)
    }
}

module.exports = OpError