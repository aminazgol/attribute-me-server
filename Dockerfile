# syntax=docker/dockerfile:1
FROM node:14.15.4 as base

WORKDIR /code

COPY ["package.json", "package-lock.json", "./"]

FROM base as prod
RUN npm ci
COPY . .
CMD [ "npm", "run", "start" ]