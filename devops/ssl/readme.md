## run
first comment the 443 server on nginx template so certbot can create certificates and nginx won't crash
after first run uncomment it and restart the ssl should work

## Automatically renew SSL certificate
in order to renew the ssl every 90 days add the following cronjob to the server
first
```
chmod +x ssl_renew.sh
```
and then run `sudo crontab -e`
and add the following line to the end
```
0 12 * * * cd /home/gitlab-runner/builds/14dHxzxU/0/aminazgol/attribute-me-server/devops/ssl/&&./ssl_renew.sh >> /var/log/cron.log 2>&1
```