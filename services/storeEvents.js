const {fetchEventsByDateParsed, getEventLabelAndCategoryMap} = require('../analytics-api/events');
const EventDB = require('../db/EventDB');
const DBError = require('../error/DBError');
const { eventCategory } = require('../db/GaToDBEventMapper');
const utils = require('../utils');
const DB = require('../db/DB');
async function storeEvents(startDate = "30daysAgo", endDate ="today"){
    try{

        var offset = 0
        var rowCount = 1
        
        const db = new DB()
        const eventDB = new EventDB()
        
        var duplicateEvents = 0,
        newEvents = 0


        while(offset < rowCount){
            /* save when this loop started */
            var thisLoopStartTime = Date.now()
            
            var res = await fetchEventsByDateParsed(startDate, endDate, null, offset)
            
            if(offset === 0)
                console.log(`storeEvents: Fetching ${res.rowCount} event records from ${startDate} to ${endDate}, started`)

            if(!res.rows || res.rows.length === 0)
                throw new Error("No rows returned from fetch while the 'offset' is less than 'rowCount'")
            
            /* avoid error: relation `events` does not exists */
            await db.useAttributeSchema()
            
            /* add events until reach a new event record */
            var newEventIndex = null
            for(var record of res.rows){
                var result = await eventDB.insertOrUpdateDuplicate(record)
                
                if(result.isDuplicate){
                    duplicateEvents ++;
                }
                else {
                    newEventIndex = res.rows.indexOf(record)
                    newEvents ++;
                    // break
                }
            }
            
            /* add the rest of the new events fast (without duplicate check) */
            /*
            if(newEventIndex !== null){
                var newRecords = res.rows.slice(newEventIndex + 1)
                var chunks = splitToChunks(newRecords, 1000)
                for(var chunk of chunks){
                    await eventDB.insertManyEvents(chunk)
                    newEvents += chunk.length;
                }
            }
            */
            rowCount = res.rowCount
            offset = offset + 10000

            await untilTimePasses(thisLoopStartTime, 1000)
        }

        console.log(`storeEvents: ${newEvents} new events added and ${duplicateEvents} were duplicated`)

    } catch(e){
        console.error("store event error", e)
        throw e
    }
}

async function addEventCategories(startDate, endDate){
    var offset = 0
    var rowCount = 1
    const eventDB = new EventDB()
    while(offset < rowCount){
        var res = await getEventLabelAndCategoryMap(startDate, endDate , offset)


        for(var row of res.rows){
            await eventDB.updateEventsCategory(row.eventLabel, row.eventCategory)
        }
        
        rowCount = res.rowCount
        offset = offset + 10000 
    }
}

async function startEventFetchingEngine(){
    while(1){
        /* save when this loop started */
        var thisLoopStartTime = Date.now()

        /* The engine main job*/
        try{

            const eventDB = new EventDB()
            var lastEvent = await eventDB.getLastEvent()
            if(!lastEvent || !lastEvent.time){            
                console.log("Database is empty, fetching data from 2 years ago")
                var startDate = "730daysAgo" // two years ago
            }
            else if(typeof lastEvent.time !== "number" && typeof lastEvent.time !== "string"){
                console.error("Event record with bad time value: ", lastEvent)
                console.log("Database is corrupted, fetching data from 2 years ago")
                var startDate = "730daysAgo" // two years ago
            }
            else {
                var yday = new Date();
                yday.setDate(yday.getDate() - 1)
                var yesterdayTime = parseInt(yday.toISOString().slice(0,10).replace(/-/g,"") + "00");
                if(lastEvent.time > yesterdayTime){
                    var startDate = "7daysAgo"
                }else {
                    var startDate = getDateStringOfEvent(lastEvent.time)
                }
            }
            await storeEvents(startDate, "today")
            
            /* refresh the Daily Events materialized view data */
            if(await eventDB.checkDailyMatViewExists()){
                console.log("Refreshing daily events")
                await eventDB.refreshDailyEvents()
            } else {
                console.log("Creating daily events materialized view")
                await eventDB.createMatViewOfDailyEvents()
            }
            console.log("Daily events updated")
            }catch(e){
                console.error('event fetching engine error ', e)
            }
            
            /* if this loop took more than 15 minutes then continue */
            
            
            await untilTimePasses(thisLoopStartTime, 15 * 60 * 1000)
        }
}

function untilTimePasses(fromTime, howLong){
    return new Promise((resolve, reject) => {
        setInterval(()=>{
            if(Date.now() >= fromTime + howLong)
                resolve()
        }, 1000)
    })
}
function getDateStringOfEvent(time){
    if(typeof time === 'string')
        var timeStr = time
    else if(typeof time === "number")
        var timeStr = time.toString()
    else
        throw new DBError("bad time value passed: " + time)

    var dateString = timeStr.substr(0,4) +"-"+ timeStr.substr(4,2) +"-"+ timeStr.substr(6,2)
    return dateString
}

function splitToChunks(array, chunkSize){
    var i,j;
    var chunks = []
    for (i = 0,j = array.length; i < j; i += chunkSize) {
        var chunk = array.slice(i, i + chunkSize);
        chunks.push(chunk)
    }
    return chunks;
}

/**
 * This functions checks if any of the Event's properties are null
 * It helps to re-fill all the database records if a new property added to analytics data
 * 
 * @param {*} event event to check
 */
function onePropertyIsNull(event){
    for(var prop of [
        'dateHour',
        'eventCategory',
        'eventCount',
        'eventLabel',
        'eventName',
        'medium',
        'campaignName',
        'source'
    ]){
        if(event[prop] === null || event[prop] === undefined)
            return true
    }
    return false
}

module.exports = {startEventFetchingEngine}