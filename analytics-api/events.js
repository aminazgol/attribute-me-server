/**
 * TODO(developer): Uncomment this variable and replace with your
 *   Google Analytics 4 property ID before running the sample.
 */
const propertyId = '257441411';

// Imports the Google Analytics Data API client library.
const { BetaAnalyticsDataClient } = require('@google-analytics/data');

// Using a default constructor instructs the client to use the credentials
// specified in GOOGLE_APPLICATION_CREDENTIALS environment variable.
const analyticsDataClient = new BetaAnalyticsDataClient();


async function fetchEventsByDate(startDate, endDate, offset=0) {

    async function runReport() {
        const [response] = await analyticsDataClient.runReport(
            {
                property: `properties/${propertyId}`,
                "dateRanges": [{ "startDate": startDate, "endDate": endDate }],
                "dimensions": [
                    { name: "dateHour" },
                    { name: "eventName" },
                    { name: "sessionMedium" },
                    { name: "sessionSource" },
                    { name: "sessionCampaignName" },
                    { name: "customEvent:event_label" },
                    { name: "fullPageUrl"},
                    { name: "pageReferrer"},
                    { name: "customEvent:event_category" },
                    // { name: "country"},

                ],
                "metrics": [
                    { "name": "eventCount" },
                    { "name": "eventValue" },
                    // { "name": "totalUsers" },
                    // { "name": "eventCountPerUser" }

                ],
                offset: offset,
                orderBys:[
                    {desc: false, dimension:{dimensionName: "dateHour"}}
                ],
            }


        );
        return response
    }

    return await runReport();
}

async function getEventLabelAndCategoryMap(startDate, endDate, offset=0){
    async function runReport() {
        const [response] = await analyticsDataClient.runReport(
            {
                property: `properties/${propertyId}`,
                "dateRanges": [{ "startDate": startDate, "endDate": endDate }],
                "dimensions": [
                    { name: "customEvent:event_category" },
                    { name: "customEvent:event_label" }
                ],
                "metrics": [
                    { "name": "eventCount" }

                ],
                offset: offset
            }
        );
        return response
    }

    var result =  await runReport();
    result.rows = result.rows.map(row=>{
        var newRow = {
            eventCategory: row.dimensionValues[0].value,
            eventLabel: row.dimensionValues[1].value
        }
        return newRow
    })

    return result
}

async function fetchEventsByDateParsed(startDate, endDate, campaignMedium, offset){
    
    var result = await fetchEventsByDate(startDate, endDate, offset);
    result.rows = result.rows.map(row => {
        var newRow = {
            dateHour: row.dimensionValues[0].value,
            eventName: row.dimensionValues[1].value,
            medium: row.dimensionValues[2].value,
            source: row.dimensionValues[3].value,
            campaignName: row.dimensionValues[4].value,
            eventLabel: row.dimensionValues[5].value,
            fullPageUrl: row.dimensionValues[6].value,
            pageReferrer: row.dimensionValues[7].value,
            eventCategory: row.dimensionValues[8].value,
            // country: row.dimensionValues[8].value,
            hostName: null,
            pagePath: null,
            pageQuery: null,
            deviceCategory: null,
            eventCount: row.metricValues[0].value,
            eventValue: row.metricValues[1].value,
            // totalUsers: row.metricValues[2].value,
            // eventCountPerUser: row.metricValues[3].value,
        }

        var {host, path, query} = fullUrlBreakUp(newRow.fullPageUrl)
        newRow.pagePath = path
        newRow.hostName = host
        newRow.pageQuery = query
        
        
        if(host.match(/(http[s]?:\/\/)?m\.thorchain\.org/)){
            newRow.deviceCategory = "Mobile"
        } else if(host.match(/(http[s]?:\/\/)?(.*.)?skip\.(exchange|com)/)) {
            newRow.deviceCategory = "Desktop/Mobile"
        } else {
            newRow.deviceCategory = "Desktop"
        }

        return newRow
    });

    /* drop any data that is not thorchain.org or skip.exchange */
    result.rows.filter(element =>{    
        var match = (
            element.hostName.match(/(http[s]?:\/\/)?(.*.)?skip\.(exchange|com)/) 
            || 
            element.hostName.match(/(http[s]?:\/\/)?(.+\.)?thorchain\.org/)
            )
        if(match) return true
        else return false
    })

    return result
}

function fullUrlBreakUp(fullPageUrl){
    var host = null,
    path = null,
    query = null

    try{

        host = fullPageUrl.match(/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)/)[0]
        var pathPlusQuery = fullPageUrl.split(host)[1].split('?')
        path = pathPlusQuery[0]
        query = pathPlusQuery[1]

    } catch(e){}
    return {host, path, query}
}

module.exports = {fetchEventsByDate, fetchEventsByDateParsed, getEventLabelAndCategoryMap}