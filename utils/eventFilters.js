const { query } = require("express")

var eventFilters = {
    'THORChain.org': {
        'All Data': {
            categories: []
        },
        'Website Loads': {
            names: ['first_visit', 'page_view', 'session_start']
        },
        'Internal Links': {
            names: ['internal_link']
        },
        'External Links': {
            names: ['external_link']
        }    
    },

    'SKIP.exchange': {
        'All Data': {
            categories: []
        },
        'Interface Loads': {
            names: ['page_view']
        },
        'New Wallets': {
            labels: ['button_understand']
        },
        'Swap Preps': {
            queryCondition: `
            event_label like '%button_swap%'
            AND event_label like '%usd%'
            AND event_label NOT like '%confirm%'
            AND event_label NOT like '%fee%'
            AND event_label NOT like '%slip%'
            `
        },
        'Swaps':{
            queryCondition: `
            event_label like '%button_swap_confirm%'
            AND event_label NOT like '%fee%'
            AND event_label NOT like '%slip%'
            `
        },
        'RUNE Upgrade':{
            queryCondition: `
            event_label like '%button_swap_confirm%'
            AND event_label NOT like '%fee%'
            AND event_label NOT like '%slip%'
            `
        },
        'Deposits':{
            
            queryCondition: `
            event_label like '%button_deposit_confirm%'
            AND event_label NOT like '%fee%'
            AND event_label NOT like '%slip%'
            `
        },
        'Withdrawal Preps':{
            queryCondition: `
            event_label like '%button_withdraw%'
            AND event_label like '%usd%'
            AND event_label NOT like '%confirm%'
            AND event_label NOT like '%fee%'
            AND event_label NOT like '%slip%'
            `
        },
        'Withdrawals':{
            queryCondition: `
            event_label like '%button_withdraw_confirm%'
            AND event_label NOT like '%fee%'
            AND event_label NOT like '%slip%'
            `
        },
        'Network Fees':{
            queryCondition: `
            event_label like '%confirm%'
            AND event_label like '%fee_usd%'
            `
        },
        'Slips':{
            queryCondition: `
            event_label like '%confirm%'
            AND event_label like '%slip%'
            `
        }
    }
}

function getQueryCondition(url, filterName){
    var queryCondition = ''
    var filter = null

    if(url.includes('thorchain.org') || url.includes('THORChain.org')){
        queryCondition += "full_page_url like '%' || 'thorchain.org' || '%' "
        filter = eventFilters['THORChain.org'][filterName]
    } else if (url.includes('skip.exchange') || url.includes('SKIP.exchange')){
        queryCondition += "full_page_url like '%' || 'skip.exchange' || '%' "
        filter = eventFilters['SKIP.exchange'][filterName]
    }
    if(!filter)
    return false

    if(filter.queryCondition){
        queryCondition += " AND "
        queryCondition += filter.queryCondition
        return queryCondition
    }
    
    if(filter.names && filter.names.length > 0){
        queryCondition += " AND "
        queryCondition += "( "
        for(var i in filter.names){
            if(i != 0) queryCondition += " OR "
            queryCondition += `event_name like '%' || '${filter.names[i]}' || '%' `
        }
        queryCondition += " )"
    }
        
    if(filter.categories && filter.categories.length > 0){
        queryCondition += " AND "
        queryCondition += "( "
        for(var i in filter.categories){
            if(i != 0) queryCondition += " OR "
            queryCondition += `event_category like '%' || '${filter.categories[i]}' || '%' `
        }
        queryCondition += " )"
    }

    if(filter.labels && filter.labels.length > 0){
        queryCondition += " AND "
        queryCondition += "( "
        for(var i in filter.labels){
            if(i != 0) queryCondition += " OR "
            queryCondition += `event_label like '%' || '${filter.labels[i]}' || '%' `
        }
        queryCondition += " )"
    }
        
    return queryCondition
}

module.exports = {getQueryCondition}