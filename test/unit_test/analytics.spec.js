require('dotenv').config()
var chai = require('chai')
var assert = require('assert');
var {fetchEventsByDate, fetchEventsByDateParsed, getEventLabelAndCategoryMap} = require('../../analytics-api/events');
const { exception } = require('console');

chai.should()

chai.use(require('chai-like'));
chai.use(require('chai-things')); // Don't swap these two

const expect = chai.expect;

describe('Analytics events', function() {
    this.timeout(40000)
    it('testing chai attributes', async function(){
        [{ a: 1 }, { b: 2 }].should.not.include({ a: 1 });
        [{ a: 1 }, { b: 2 }].should.include.something.that.deep.equals({ b: 2 });
    })
    it('can fetch last week events', async function() {
        var res = await fetchEventsByDate("7daysAgo", "today")
        expect(res).to.be.an('object')
        expect(res).to.have.property('dimensionHeaders').to.be.an('array')
        expect(res).to.have.property('rows').to.be.an('array')
    });
    it('can fetch last 2 years events', async function() {
        var res = await fetchEventsByDate("720daysAgo", "today")
        expect(res).to.be.an('object')
        expect(res).to.have.property('dimensionHeaders').to.be.an('array')
        expect(res).to.have.property('rows').to.be.an('array')
    });
    it('can fetch last week events (parsed)', async function() {
        var res = await fetchEventsByDateParsed("7daysAgo", "today")
        expect(res).to.be.an('object').have.property('rows')
        expect(res).have.property('rowCount')

        // expect(res).all.include.something.like({dateHour: 'Blah'}) //Heavy process
        for(var prop of [
            'dateHour',
            'eventCategory',
            'eventCount',
            'eventLabel',
            'eventName',
            'medium',
            'campaignName',
            'source',
            'totalUsers',
            'eventCountPerUser'
        ]){
            expect(res.rows).all.have.property(prop)
        }

        /* Check records are ordered by date */
        var preRecordDate = -1
        for(var row of res.rows){
            if(parseInt(row.dateHour) < preRecordDate)
                throw `Rows are not ordered by date (${row.dateHour} < ${preRecordDate})`
            preRecordDate = parseInt(row.dateHour)
        }
        console.log("from ",res.rows[0].dateHour)
        console.log("to ",res.rows[res.rows.length - 1].dateHour)
        

    }); 
    it('can fetch events with campaignMedium filter', async function(){
        const campaignMedium = "referral"
        var res = await fetchEventsByDateParsed("7daysAgo", "today", campaignMedium)
        expect(res).to.be.an('object').have.property('rows')
        expect(res).have.property('rowCount')

        // expect(res).all.include.something.like({dateHour: 'Blah'}) //Heavy process
        for(var prop of [
            'dateHour',
            'eventCategory',
            'eventCount',
            'eventLabel',
            'eventName'
        ]){
            expect(res.rows).all.have.property(prop)
        }
        expect(res.rows).all.have.property('medium', campaignMedium)
    })

    it('can fetch all last month events page by page', async function(){
        var offset = 0
        var rowCount = 1
        var i =0;
        var j = 0;
        while(offset < rowCount){
            console.log("page: ", ++i)
            var res = await fetchEventsByDateParsed("30daysAgo", "today", null, offset)
    
            expect(res).to.be.an('object').have.property('rows')
            expect(res).have.property('rowCount')
    
            for(var prop of [
                'dateHour',
                'eventCategory',
                'eventCount',
                'eventLabel',
                'eventName',
                'medium'
            ]){
                expect(res.rows).all.have.property(prop)
            }
            
            rowCount = res.rowCount
            offset = offset + 10000 
            j += res.rows.length;
        }
        expect(j).to.be.eq(rowCount)
        
    })

    it('it can get the map between event_category and event_label', async function(){
        var offset = 0
        var rowCount = 1
        var i =0;
        var j = 0;
        while(offset < rowCount){
            console.log("page: ", ++i)
            var res = await getEventLabelAndCategoryMap("30daysAgo", "today", offset)
            
            expect(res).to.be.an('object').have.property('rows')
            expect(res).have.property('rowCount')
    
            for(var prop of [
                'eventCategory',
                'eventLabel',
            ]){
                expect(res.rows).all.have.property(prop)
            }
            
            rowCount = res.rowCount
            offset = offset + 10000 
            j += res.rows.length;
        }
        expect(j).to.be.eq(rowCount)
        
    })

    it('check if campaignName has incorrect value', async ()=>{
        var offset = 0
        var rowCount = 1
        var i =0;
        var j = 0;
        while(offset < rowCount){
            console.log("page: ", ++i)
            var res = await fetchEventsByDateParsed("1daysAgo", "today", null, offset)
    
            expect(res).to.be.an('object').have.property('rows')
            expect(res).have.property('rowCount')
            
            for (var row of res.rows){
                for(var value of [
                    'article',
                    'arbitrageur_tools',
                    'community_roles',
                    'doc',
                    'interfaces',
                    'liquidity_providers',
                    'liquidity-provider-tools',
                    'pool_disconnected'
                ]){
                    if(row.campaignName == value){
                        throw Error("CampaignName has value= " + value)
                    }
                }
            }
            rowCount = res.rowCount
            offset = offset + 10000 
            j += res.rows.length;
        }
        expect(j).to.be.eq(rowCount)
    })
});