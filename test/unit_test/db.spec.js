require('dotenv').config()
const DB = require('../../db/DB')
const EventDB = require('../../db/EventDB')
const { expect } = require('chai')

const events = [
    {
        dateHour: "2021020400",
        eventName: "page_view",
        medium: "(not set)",
        source: "(not set)",
        campaignName: "(not set)",
        eventCategory: "(not set)",
        eventLabel: "(not set)",
        fullPageUrl: "thorchain.org/",
        pageReferrer: "",
        hostName: "thorchain.org",
        pagePath: "/",
        pageQuery: undefined,
        eventCount: "36",
        eventValue: "0",
        totalUsers: "31",
        eventCountPerUser: "1.1612903225806452",
    },
    {
        dateHour: "2022220400",
        eventName: "page_viewww",
        medium: "(not set)",
        source: "(not set)",
        campaignName: "(not set)",
        eventCategory: "(not set)",
        eventLabel: "(not set)",
        fullPageUrl: "thorchain.org/",
        pageReferrer: "",
        hostName: "thorchain.org",
        pagePath: "/",
        pageQuery: undefined,
        eventCount: "8765",
        eventValue: "0",
        totalUsers: "31",
        eventCountPerUser: "1.1612903225806452",
    }
]

describe("DB connection", function () {
    it("can connect to the db properly", async function () {
        const db = new DB()
    })
    it("can initialize schema structure", async function () {
        const db = new DB()
        await db.init()
    })
})
describe('Event DB', function (){

    it("can insert one event", async function () {
        const eventDB = new EventDB()
        const res = await eventDB.insertEvent(events[0])
        expect(res).to.be.an('object').have.property('id')
    })

    it("can insert multiple events", async function () {
        const eventDB = new EventDB()
        const res = await eventDB.insertManyEvents(events)
        expect(res).to.be.an('object').have.property('id')
    })
    
    it("check for duplicate", async function(){
        const testEvent = {
            dateHour: "2021080410",
            medium: "(not set)",
            eventName: "page_view",
            eventCategory: "(not set)",
            eventLabel: "(not set)",
            eventCount: "226",
            eventValue: 300

        }
        const eventDB = new EventDB()
        await eventDB.insertEvent(testEvent)
        const duplicate = await eventDB.findDuplicate(testEvent)
        expect(duplicate).to.not.equal(false)
        expect(duplicate).to.be.a('object').have.property('id')
    })
    
    it("can update an event", async function(){
        const testEvent = {
            dateHour: "2021080410",
            medium: "(not set)",
            eventName: "page_view",
            eventCategory: "(not set)",
            eventLabel: "(not set)",
            eventCount: "226",
            eventValue: 300

        }
        const eventDB = new EventDB()
        var newEvent = await eventDB.insertEvent(testEvent)
        var res = await eventDB.updateEventCount(newEvent.id, 2000)
        expect(res).to.be.an('object').have.property('id')
    })
    
    it("can insert an event or update if the event is duplicated", async function(){
        const testEvent = {
            dateHour: Date.now(),
            medium: "(not set)",
            eventName: "page_view",
            eventCategory: "(not set)",
            eventLabel: "(not set)",
            eventCount: "226",
            eventValue: 300

        }
        const eventDB = new EventDB()
        
        /* test insert functionality for a new event record */
        var res = await eventDB.insertOrUpdateDuplicate(testEvent)
        var newEvent = res.event
        expect(newEvent).to.be.an('object').have.property('id')
        expect(newEvent).have.property('event_count', testEvent.eventCount)
        expect(res.isDuplicate).to.be.eq(false)
        
        /* change testEvent count to test update functionality*/
        testEvent.eventCount = 300
        res = await eventDB.insertOrUpdateDuplicate(testEvent)
        var updatedEvent = res.event
        expect(updatedEvent).to.be.an('object').have.property('id', newEvent.id)
        expect(updatedEvent).have.property('event_count', '300')
        expect(res.isDuplicate).to.be.eq(true)
    })

    it("should throw a friendly error when a very large event_count value can't be added", async ()=>{
        const testEvent = {
            dateHour: Date.now(),
            medium: "(not set)",
            eventName: "page_view",
            eventCategory: "(not set)",
            eventLabel: "(not set)",
            eventCount: "12345678912",
            eventValue: 300
        }
        const eventDB = new EventDB()
        try{
            await eventDB.insertOrUpdateDuplicate(testEvent)
            throw new Error("it didn't throw any error")
        } catch(e){
            expect(e).to.haveOwnProperty("message", "failed to insert or update new record: value \"12345678912\" is out of range for type integer")
        }

    })

    it('can get total values', async()=>{
        const eventDB = new EventDB()
        var total = await eventDB.getTotalValues(2021041316, 2021050216)
        for(var prop of [
            'total_count',
            'total_value',
            'total_total_users',
            'total_event_count_per_user'
        ]){
            expect(total).have.property(prop)
            expect(parseFloat(total[prop])).to.be.a('number')
        }

    })


    it('can get events with filters', async()=>{
        startDate =  2021020200
        endDate = 2021080220
        var filters = {
            medium : "organic",
            campaign_name: "(organic)",
            event_name: 'session_start',
            event_category: '(not set)',
            event_label: '(not set)',
            source: 'google'
        }
        const eventDB = new EventDB()
        for(var daily of [true, false]){

            var res = await eventDB.getEventsAndTotalsByFilters(startDate, endDate, filters, daily)
            expect(res.events).to.be.an('array')
            
            var totalCount = 0
            for(var event of res.events){
                for(var key in filters){
                    expect(event).have.property(key).to.be.eq(filters[key])
                }
                totalCount+=parseInt(event.event_count)
            }
            expect(res.totals.total_count == totalCount).to.be.eq(true)
        }
    })

    it('can a query condition based on the filter string', (done)=>{
        var url = 'thorchain.org',
            filter = 'Tool'
        var {getQueryCondition} = require('../../utils/eventFilters')
        var queryCondition = getQueryCondition(url, filter)
        expect(queryCondition).to.be.a('string').eq("full_page_url like '%' || 'thorchain.org' || '%'  AND ( event_category like '%' || 'tool' || '%'  )")

        queryCondition = getQueryCondition('SKIP.exchange', 'Interface Loads')
        expect(queryCondition).to.be.a('string').eq("full_page_url like '%' || 'skip.exchange' || '%'  AND ( event_name like '%' || 'first_visit' || '%'  OR event_name like '%' || 'page_view' || '%'  OR event_name like '%' || 'session_start' || '%'  )")
        done()
    })
    it('can generate a query string and query value array from one event', async ()=>{
        var {queryString, queryValues} = new EventDB().generateQueryStringAndValues(events[0])
        expect(queryString).to.be.a('string').eq("INSERT INTO events ( time, event_name, medium, source, campaign_name, event_category, event_label, full_page_url, host_name, page_path, page_referrer, event_count, event_value, total_users, event_count_per_user ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING *")
        expect(queryValues).to.eql([
            "2021020400",
            "page_view",
            "(not set)",
            "(not set)",
            "(not set)",
            "(not set)",
            "(not set)",
            "thorchain.org/",
            "thorchain.org",
            "/",
            "",
            "36",
            "0",
            "31",
            "1.1612903225806452",
          ])

    })

    it('can generate a query string and query value array from multiple events', async ()=>{
        

        var {queryString, queryValues} = new EventDB().generateInsertManyQueryStringAndValue(events)
        expect(queryString).to.be.a('string').eq(
            "INSERT INTO events ( time, event_name, medium, source, campaign_name, event_category, event_label, full_page_url, host_name, page_path, page_referrer, event_count, event_value, total_users, event_count_per_user ) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) , ( $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30) RETURNING *;"
        )
        expect(queryValues).to.eql([
            "2021020400",
            "page_view",
            "(not set)",
            "(not set)",
            "(not set)",
            "(not set)",
            "(not set)",
            "thorchain.org/",
            "thorchain.org",
            "/",
            "",
            "36",
            "0",
            "31",
            "1.1612903225806452",
            "2022220400",
            "page_viewww",
            "(not set)",
            "(not set)",
            "(not set)",
            "(not set)",
            "(not set)",
            "thorchain.org/",
            "thorchain.org",
            "/",
            "",
            "36",
            "0",
            "31",
            "1.1612903225806452",
          ])

    })

    it('can check daily mat view exists', async()=>{
        var res = await (new EventDB()).checkDailyMatViewExists()
        console.log(res)
    })
})