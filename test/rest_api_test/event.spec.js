const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../index')
const { expect } = require('chai')

chai.use(chaiHttp)

describe("Events REST API", function(){
    it("can fetch last week events Hourly", (done)=>{
        var todayDate = new Date()
        var hour = todayDate.getHours()
        var day = todayDate.getDate()
        var month = todayDate.getMonth()
        var year = todayDate.getFullYear()

        var lastWeekDay = day - 7
        var lastWeekMonth = month
        if(lastWeekDay < 1){
            lastWeekDay = 30 + (day - 7)
            lastWeekMonth = month -1
        }
        
        var startDate = year+""+lastWeekMonth+lastWeekDay+hour
        var endDate = year+""+lastWeekMonth+lastWeekDay+hour

        startDate =  2021020200
        endDate = 2021020220

        chai.request(server).get(`/events?startDate=${startDate}&endDate=${endDate}&daily=false`)
        .end((err, res)=>{
            if(err){
                throw err
            }
            res.should.have.status(200)
            res.body.should.have.property('events').to.be.an('array')
            res.body.events.length.should.be.gt(0)
            res.body.should.have.property('totals').to.be.an('object')
            for(var prop of [
                "time",
                "medium",
                "campaign_name",
                "source",
                "event_category",
                "event_label",
                "event_name",
                // "country",
                "device_category",
                "page_referrer",
                "event_count",
                "event_value",
                // "total_users",
                // "event_count_per_user"
            ]){
                res.body.events.should.all.have.property(prop)
            }
            for(var prop of [
                'total_count',
                'total_value',
                // 'total_total_users',
                // 'total_event_count_per_user'
            ]){
                res.body.totals.should.have.property(prop)
            }
            done()
        })

    })
    it("can fetch last week events by filters", (done)=>{
        
        var startDate =  2021020200
        var endDate = 2021040220
        var filters = {
            website: 'thorchain.org',
            filterName: 'Website Loads'
        }
        var responseCount = 0
        for(var daily of [true, false]){
            var url = `/events?startDate=${startDate}&endDate=${endDate}
            &filter_website=${filters.website}
            &present_filter=${filters.filterName}&daily=${daily}`

            chai.request(server).get(url)
            .end((err, res)=>{
                res.should.have.status(200)
                res.body.should.have.property('events').to.be.an('array')
                res.body.should.have.property('totals').to.be.an('object')
                for(var event of res.body.events){
                    
                    if(event.event_name != 'first_visit' && event.event_name != 'page_view' && event.event_name != 'session_start' )
                        throw new Error('event_name doesn\'t match the filter')
                    
                    responseCount++
                    if(responseCount == 2)
                        done()
        
                }
            })
        }

    })
    it("return 400 on wrong date type", (done)=>{
        chai.request(server).get(`/events?startDate=10&endDate=e`)
        .end((err, res)=>{
            res.should.have.status(400)
            res.body.should.haveOwnProperty("message", "Bad Request!: \"endDate\" must be a number")
            done()
        })
    })

})