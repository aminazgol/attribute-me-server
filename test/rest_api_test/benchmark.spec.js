describe('benchmarking', ()=>{
    it('benchmark /events', (done)=> {
        
        function requestEvents(startDate, endDate){
            return new Promise((resolve, reject) =>{

                var url = `http://localhost:3001/events?startDate=${startDate}&endDate=${endDate}&daily=false`
                
                http.get(url, (res)=>{
                    resolve(res)
                })
            })
        }
        var i=0;
        setInterval(async ()=>{
            var res = await requestEvents(2021102023, 2021102323)
            i++
            if(res.statusCode !== 200)
                throw Error("Status: ", res.status)
            if(i%10 == 0){
                const formatMemoryUsage = (data) => `${Math.round(data / 1024 / 1024 * 100) / 100} MB`

                const memoryData = process.memoryUsage()
                
                const memoryUsage = {
                                rss: `${formatMemoryUsage(memoryData.rss)} `,//-> Resident Set Size - total memory allocated for the process execution
                                heapTotal: `${formatMemoryUsage(memoryData.heapTotal)}`,// -> total size of the allocated heap`,
                                heapUsed: `${formatMemoryUsage(memoryData.heapUsed)}`,// -> actual memory used during the execution`,
                                external: `${formatMemoryUsage(memoryData.external)}`// -> V8 external memory`,
                }
                
                console.log(memoryUsage)
            }

        }, 200)
    })
})