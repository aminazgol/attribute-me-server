/* Connect database (PostgreSql) and create schemas */

const { Pool, Client } = require('pg')
const DBError = require('../error/DBError')

const pool = (() => {
    try{
        if (process.env.NODE_ENV !== 'production') {
            return new Pool({
                connectionString: process.env.DATABASE_URL,
                ssl: false
            });
        } else {
            return new Pool({
                connectionString: process.env.DATABASE_URL,
                ssl: {
                    rejectUnauthorized: false
                }
            });
        }
    }catch(e){
        throw new DBError('failed to start connection pool', e)
    }
})();

class DB {
    pool = null
    constructor() {
        this.pool = pool
    }

    /**
     * 
     * @param {string} q SQL query string
     */
    async query(q) {
        return await this.pool.query(q)
    }

    async init() {
        try{
 
            
            /* create 'attribute' Schema */ 
            await this.query("CREATE SCHEMA IF NOT EXISTS attribute")

            /* use 'attribute' schema */
            await this.query(`SET search_path TO attribute,public`)
            
            /* Create `events` Table */
            await this.query("CREATE TABLE IF NOT EXISTS events (id SERIAL PRIMARY KEY)")
            
            /* Add `events` Columns if not exists */
            const GA_TO_DB_EVENT_MAPPER = require('./GaToDBEventMapper.js')
            for(var key in GA_TO_DB_EVENT_MAPPER){
                var {dbField, type} = GA_TO_DB_EVENT_MAPPER[key]
                await this.query(`ALTER TABLE events ADD COLUMN IF NOT EXISTS ${dbField} ${type};`)
            }

            /* Create an INDEX on events time */
            await this.query('CREATE INDEX IF NOT EXISTS event_time on events(time);')
            
            /* Create Materialized view of Daily Events */
            // await this.createMatViewOfDailyEvents()


            
        }catch(e){
            throw new DBError("Database init failed", e)
        }
    }
    async useAttributeSchema(){
        await this.query("set search_path to attribute,public;")
    }
    async checkDailyMatViewExists(){
        var q = `SELECT EXISTS( select relname, relkind
            from pg_class
            where relname = 'daily_events_matview'
            and relkind = 'm');`
        var res = await this.pool.query(q)
        if(res.rows && res.rows[0] && res.rows[0].exists)
            return res.rows[0].exists
        else
            return false
    }
    async createMatViewOfDailyEvents(){
        var q = `CREATE MATERIALIZED VIEW IF NOT EXISTS daily_events_matview AS
        select
            cast(substring(cast(time as text), 0, 9) as integer) as date,
            country,
            device_category,
            host_name,
            page_path,
            page_query,
            page_referrer,
            full_page_url,
            medium,
            source,
            campaign_name,
            event_name,
            event_category,
            event_label,
            sum(event_count) as event_count,
            sum(event_value) as event_value
            from events
    
            group by
            date,
                    medium,
                    campaign_name,
                    source,
                    event_name,
                    event_category,
                    event_label,
                    country,
                    device_category,
                    page_referrer,
                    host_name,
                    page_query,
                    full_page_url,
                    page_path;`

        var res = await this.pool.query(q)
        return res
    }

    async refreshDailyEvents(){
        var res = await this.pool.query('REFRESH MATERIALIZED VIEW daily_events_matview;')
        return res
    }
}
function getDbName(connectionString){
    var a = connectionString.split('/')
    var dbName = a[a.length - 1]
    return dbName
}
module.exports = DB