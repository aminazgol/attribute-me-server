const DB = require('./DB')
const DBError = require('../error/DBError')
const EventFilters = require('../utils/eventFilters')
const OpError = require('../error/OpError')
const GA_TO_DB_EVENT_MAPPER = require('./GaToDBEventMapper')


class EventDB extends DB {
    /**
     * insert one event
     * @param {Object} event Event row to be inserted 
     */
    async insertEvent(event) {
        try {
            var { queryString, queryValues } = this.generateQueryStringAndValues(event)

            var res = await this.pool.query(queryString, queryValues)
            if (res.rowCount <= 0) return false
            return res.rows[0]
        } catch (e) {
            throw new DBError('failed to insert new record', e)
        }
    }

    async insertManyEvents(events) {
        try {
            var { queryString, queryValues } = this.generateInsertManyQueryStringAndValue(events)

            var res = await this.pool.query(queryString, queryValues)
            if (res.rowCount <= 0) return false
            return res.rows
        } catch (e) {
            throw new DBError('failed to insert new record', e)
        }
    }

    async deleteAllDuplicates(){
        var q = `
        DELETE FROM attribute.events a USING (
            SELECT MIN(ctid) as ctid, time
              FROM events
              GROUP BY time HAVING COUNT(*) > 1
            ) b
            WHERE a.time = b.time
            AND a.ctid <> b.ctid
        `
    }

    /**
     * Find an event with the same fields as the input event (except 'eventCount') -> Deprecated 
     * @param {Object} event event to find in current data
     */
    async findDuplicate(event) {
        var query = {
            text: `
            SELECT * FROM attribute.events WHERE
                time=$1 AND
                medium=$2 AND
                event_category=$3 AND
                event_label=$4 AND
                event_name=$5 AND
                campaign_name=$6 AND
                source=$7 AND
                full_page_url=$8 AND
                page_referrer=$9
            `,
            values: [
                event.dateHour,
                event.medium,
                event.eventCategory,
                event.eventLabel,
                event.eventName,
                event.campaignName,
                event.source,
                event.fullPageUrl,
                event.pageReferrer
            ]
        }
        const res = await this.pool.query(query)

        if (res.rowCount <= 0)
            return false
        else
            return res.rows[0]
    }
    generateQueryStringAndValues(event) {
        var queryString = "",
            queryValues = []
        /* creating query string */
        queryString += "INSERT INTO attribute.events ( "
        var eventKeys = Object.keys(GA_TO_DB_EVENT_MAPPER)
        for (var key of eventKeys) {
            if (eventKeys.indexOf(key) !== 0) {
                queryString += ", "
            }
            queryString += GA_TO_DB_EVENT_MAPPER[key].dbField
            queryValues.push(event[key])
        }
        queryString += " ) VALUES ("
        for (var i in eventKeys) {
            if (i !== '0') {
                queryString += ", "
            }
            queryString += "$" + (parseInt(i) + 1)
        }
        queryString += ") RETURNING *"
        return { queryString, queryValues }
    }

    generateInsertManyQueryStringAndValue(events) {
        var queryString = "",
            queryValues = []
        /* creating query string */
        queryString += "INSERT INTO attribute.events ( "
        var eventKeys = Object.keys(GA_TO_DB_EVENT_MAPPER)
        for (var key of eventKeys) {
            if (eventKeys.indexOf(key) !== 0) {
                queryString += ", "
            }
            queryString += GA_TO_DB_EVENT_MAPPER[key].dbField
        }
        queryString += " ) VALUES "
        var valueIndex = 1
        for (var event of events) {
            if (events.indexOf(event) !== 0) {
                queryString += ", "
            }
            queryString += "( "
            for (var i in eventKeys) {
                if (i !== '0') {
                    queryString += ", "
                }
                queryString += "$" + valueIndex
                valueIndex++

                queryValues.push(event[eventKeys[i]])

            }
            queryString += ") "
        }
        queryString += "RETURNING *;"

        return { queryString, queryValues }
    }

    /**
     *  -> Deprecated
     * @param {number} eventId 
     * @param {string} fieldToUpdate 
     * @param {*} newValue 
     */
    async updateAllEventValues(eventId, event) {
        try {

            var query = {
                text: `UPDATE attribute.events 
                SET 
                
                time=$2,
                medium=$3,
                campaign_name=$4,
                source=$5,
                event_name=$6,
                event_category=$7,
                event_label=$8,
                full_page_url=$9,
                page_referrer=$10,
                
                event_count = $11,
                event_value=$12,
                total_users=$13,
                event_count_per_user=$14
                
                WHERE id=$1
                RETURNING *`,
                values: [
                    eventId,

                    event.dateHour,
                    event.medium,
                    event.campaignName,
                    event.source,
                    event.eventName,
                    event.eventCategory,
                    event.eventLabel,
                    event.fullPageUrl,
                    event.pageReferrer,

                    event.eventCount,
                    event.eventValue,
                    event.totalUsers,
                    event.eventCountPerUser
                ]
            }
            const res = await this.pool.query(query)

            if (res.rowCount < 0) return false
            return res.rows[0]
        } catch (e) {
            throw new DBError('failed to update the record', e)
        }
    }


    async insertOrUpdateDuplicate(event) {
        try {
            var duplicate = await this.findDuplicate(event)
            if (duplicate){
                var res = await this.updateAllEventValues(
                    duplicate.id,
                    event
                    )
            }
            else
                var res = await this.insertEvent(event)
            return { event: res, isDuplicate: !(!duplicate) }
        } catch (e) {
            throw new DBError('failed to insert or update new record', e)
        }
    }

    async getEvents(startDate, endDate, campaignMedium) {
        if (!campaignMedium) {
            var query = {
                text: "select * from attribute.events where time > $1 and time < $2",
                values: [startDate, endDate]
            }
        } else {
            var query = {
                text: "select * from attribute.events where time > $1 and time < $2 and medium = $3",
                values: [startDate, endDate, campaignMedium]
            }
        }
        try {
            var res = await this.pool.query(query)
            return res.rows

        } catch (e) {
            throw new DBError(`getEvents: failed to get events with query [${startDate, endDate, campaignMedium}]`, e)
        }
    }
    async getEventsAndTotalsByFilters(startDate, endDate, filter_website, present_filter, filters, daily) {
        var queryCondition = ""
        if (filter_website) {
            queryCondition += " AND "
            var filterCondition = EventFilters.getQueryCondition(filter_website, present_filter)
            if (!filterCondition) throw new OpError(400, 'filter_website + present_filter not found!')
            queryCondition += filterCondition
        }
        var conditionValues = []
        if (filters) {

            var i = 3
            for (var prop of [
                'medium',
                'event_label',
                'event_name',
                'event_category',
                'source',
                'campaign_name',
            ]) {
                if (filters[prop]) {
                    queryCondition += ` and LOWER(${prop}) like '%' || LOWER($${i}) || '%' `
                    conditionValues.push(filters[prop])
                    i++
                }
            }

        }

        var queryText = `select
        ${daily? 'date' : 'time'},
        device_category,
        host_name,
        page_path,
        page_referrer,
        medium,
        campaign_name,
        source,
        event_name,
        event_category,
        event_label,
        event_count,
        event_value
        
        from attribute.${daily ? 'daily_events_matview': 'events'}
        
        ${daily ? 'where date >= $1 and date <= $2' : 'where time >= $1 and time <= $2'}
         
        ${queryCondition}`

    
        var dailyStartDate = ('' + startDate).substr(0,8),
        dailyEndDate = ('' + endDate).substr(0,8)
        var query = {
            text: queryText,
            values: [
                (daily? dailyStartDate :startDate),
                (daily? dailyEndDate : endDate),
                ...conditionValues
            ]
        }

        try {
            var res = await this.pool.query(query)

            var totals = await this.getTotalValues(startDate, endDate, queryCondition, conditionValues)
            return { events: res.rows, totals }

        } catch (e) {
            throw new DBError(`getEventsWithFilters: failed to get events with query [${startDate}, ${endDate}, ${filters}]`, e)
        }
    }
    getHourlyQuery(queryCondition) {
        var q = `select
        time,
        country,
        device_category,
        host_name,
        page_path,
        page_referrer,
        medium,
        campaign_name,
        source,
        event_name,
        event_category,
        event_label,
        event_count,
        event_value
        
        from attribute.events
        
        where time > $1 and time < $2 
        ${queryCondition}`
        return q
    }
    getDailyGroupedQuery(queryCondition) {
        var q = `select
        substring(cast(time as text), 0, 9) as date,
        country,
        device_category,
        host_name,
        page_path,
        page_referrer,
        medium,
        source,
        campaign_name,
        event_name,
        event_category,
        event_label,
        sum(event_count) as event_count,
        sum(event_value) as event_value
        from attribute.events
        
        where time > $1 and time < $2 
        ${queryCondition}

 group by
  date,
        medium,
        campaign_name,
        source,
        event_name,
        event_category,
        event_label,
        country,
        device_category,
        page_referrer,
        host_name,
        page_path;`
        return q
    }

    async getLastEvent() {
        var query = {
            text: "select * from attribute.events order by time desc limit 1"
        }
        try {
            var res = await this.pool.query(query)
            if (res.rowCount === 0)
                return null
            return res.rows[0]
        }
        catch (e) {
            throw new DBError("getLastEvent: failed to fetch last event")
        }
    }
    async getTotalValues(startDate, endDate, queryCondition, conditionValues) {
        var query = {
            text: `
            select
                sum(event_count) as total_count,
                sum(event_value) as total_value

            from attribute.events where time > $1 and time < $2 
            `+ queryCondition,
            values: [
                startDate,
                endDate,
                ...conditionValues
            ]
        }
        try {
            var res = await this.pool.query(query)

            return res.rows[0]

        } catch (e) {
            throw new DBError(`getEvents: failed to get events with query [${startDate, endDate}]`, e)
        }
    }

    async updateEventsCategory(eventLabel, eventCategory){
        var query = {
            text: 'UPDATE attribute.events SET event_category= $1 WHERE event_label= $2 AND event_category IS NULL;',
            values: [eventCategory, eventLabel]
        }
        var result = await this.pool.query(query)
        return result
    }
}

module.exports = EventDB