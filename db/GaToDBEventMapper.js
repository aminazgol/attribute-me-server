const GA_TO_DB_EVENT_MAPPER = {
    dateHour: {
        dbField: 'time',
        type: 'INTEGER',
        role: 'dimension'
    },
    eventName: {
        dbField: 'event_name',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    medium: {
        dbField: 'medium',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    source: {
        dbField: 'source',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    campaignName: {
        dbField: 'campaign_name',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    eventCategory: {
        dbField: 'event_category',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    eventLabel: {
        dbField: 'event_label',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    fullPageUrl: {
        dbField: 'full_page_url',
        type: 'VARCHAR(500)',
        role: 'dimension'
    },
    hostName: {
        dbField: 'host_name',
        type: 'VARCHAR(300)',
        role: 'derived_dimension'
    },
    pagePath: {
        dbField: 'page_path',
        type: 'VARCHAR(300)',
        role: 'derived_dimension'
    },
    pageQuery: {
        dbField: 'page_query',
        type: 'VARCHAR(300)',
        role: 'derived_dimension'
    },
    deviceCategory: {
        dbField: 'device_category',
        type: 'VARCHAR(300)',
        role: 'derived_dimension'
    },
    pageReferrer: {
        dbField: 'page_referrer',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    country: {
        dbField: 'country',
        type: 'VARCHAR(300)',
        role: 'dimension'
    },
    eventCount: {
        dbField: 'event_count',
        type: 'INTEGER',
        role: 'metric'
    },
    eventValue: {
        dbField: 'event_value',
        type: 'double precision',
        role: 'metric'
    },
    totalUsers: {
        dbField: 'total_users',
        type: 'double precision',
        role: 'metric'
    },
    eventCountPerUser: {
        dbField: 'event_count_per_user',
        type: 'double precision',
        role: 'metric'
    }
}
module.exports = GA_TO_DB_EVENT_MAPPER;