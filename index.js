const express = require('express')
require('dotenv').config()
var cors = require('cors')
const joi = require('joi');
const { fetchEventsByDateParsed } = require('./analytics-api/events');
const EventDB = require('./db/EventDB');
const OpError = require('./error/OpError');
const Joi = require('joi');
const app = express()


app.use(cors())

const port = process.env.SERVER_PORT



app.get('/events', async (req, res, next) =>{
    const { error } = joi.object({
      startDate: joi.number().required(),
      endDate: joi.number().required(),
      filters: joi.string().allow(null, ''),
      filter_website: joi.string().allow(null, ''),
      present_filter: joi.string().allow(null, ''),
      daily: Joi.bool().allow(null, '')
    }).validate(req.query)
    try{
      var filters = null
      if(req.query.filters){
        try{
          filters = JSON.parse(req.query.filters)
        }
        catch(e){
          throw new OpError(400, "'filters' attribute must be a JSON object")
        }
      }
      if (error)
        throw new OpError(400, "Bad Request!", error)

      var startDate = req.query.startDate
      var endDate = req.query.endDate
      try{
        var daily = JSON.parse(req.query.daily)
        
        if(typeof daily !== 'boolean') 
          throw new Error()
          
      } catch(e){
        throw new OpError(400, "'daily' should be boolean")
      }
      var filterWebsite = req.query.filter_website
      var presentFilter = req.query.present_filter

      const eventDB = new EventDB()
      var result =  await eventDB.getEventsAndTotalsByFilters(startDate, endDate,
         filterWebsite, presentFilter, filters, daily)
      
      res.status(200).json(result)
      
    } catch(err){
        next(err) 
    }
      
});

/* error handler */
app.use(function (err, req, res, next) {
  if(err instanceof OpError){
    return res.status(err.code).json({message: err.message})
  }
  console.error(err)
  res.status(500).json({message: "internal error"})
})

app.get('/', (req, res) => {
  res.send('Hello World!  ')
});

(async function startServer() {
  /* initialize database schema, table & columns */
  const DB = require('./db/DB')
  await (new DB()).init()
  console.log("Database initialization complete")

  /* start event fetching engine */
  const {startEventFetchingEngine} = require('./services/storeEvents')
  startEventFetchingEngine()

  /* start listening */
  app.listen(port, () => {
    console.log(`Attribute app listening at http://localhost:${port}`)
  })
})();



module.exports = app // for testing