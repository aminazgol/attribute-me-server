# attribute-me-server
Decentralized marketing platform
## Installation
Start Attribute-Me containers
```docker compose up -d```
### Monitoring
Install [Dockptom](https://github.com/stefanprodan/dockprom) montioring toolbox
```
cd devops/dockprom
git submodules init
git submodules update
```
Start Dockprom containers ([doc](https://github.com/stefanprodan/dockprom#install))
```
ADMIN_USER=admin ADMIN_PASSWORD=admin ADMIN_PASSWORD_HASH=JDJhJDE0JE91S1FrN0Z0VEsyWmhrQVpON1VzdHVLSDkyWHdsN0xNbEZYdnNIZm1pb2d1blg4Y09mL0ZP docker-compose up -d
```
Start telegram bot for alert manager ([doc](https://github.com/metalmatze/alertmanager-bot)
```
TELEGRAM_ADMIN=1116041467 TELEGRAM_TOKEN=2134064081:AAE_p2oVvc7o5yK8G7eeQn5mmmSO3XGecbU docker-compose up -d
```
